/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import javax.swing.JFileChooser;
import Negocio.SopaBinaria;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Carlos
 */
public class GuiBinaria extends javax.swing.JFrame {

    SopaBinaria mySopaBinaria;

    public GuiBinaria() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnCargar = new javax.swing.JButton();
        btnNumero = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtImprimir = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        NumeroDecimal = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Sopa Binaria");

        btnCargar.setText("Cargar Excel Con la Sopa ");
        btnCargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    btnCargarActionPerformed(evt);
                } catch (Exception ex) {
                    Logger.getLogger(GuiBinaria.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        btnNumero.setText("Buscar un número decimal");
        btnNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    btnNumeroActionPerformed(evt);
                } catch (Exception ex) {
                    Logger.getLogger(GuiBinaria.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        txtImprimir.setColumns(20);
        txtImprimir.setRows(5);
        txtImprimir.setText("Se encontro del número decimal XXXX en binario : YYYY \nXXXX incidencias.");
        jScrollPane1.setViewportView(txtImprimir);

        jButton1.setText("Imprimir Resultado en PDF");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jButton1ActionPerformed(evt);
                } catch (Exception ex) {
                    Logger.getLogger(GuiBinaria.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        NumeroDecimal.setText("17");
        NumeroDecimal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NumeroDecimalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(62, 62, 62)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 465, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel1)
                                                                .addGap(189, 189, 189))))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(81, 81, 81)
                                                .addComponent(btnCargar)
                                                .addGap(67, 67, 67)
                                                .addComponent(jButton1)))
                                .addContainerGap(59, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(73, 73, 73)
                                .addComponent(NumeroDecimal)
                                .addGap(18, 18, 18)
                                .addComponent(btnNumero)
                                .addGap(87, 87, 87))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnNumero)
                                        .addComponent(NumeroDecimal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnCargar)
                                        .addComponent(jButton1))
                                .addGap(19, 19, 19))
        );

        pack();
    }// </editor-fold>                        

    private void NumeroDecimalActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void btnCargarActionPerformed(java.awt.event.ActionEvent evt) throws Exception {
        JFileChooser filechoose = new JFileChooser();
        int opcion = filechoose.showOpenDialog(this);
        if (opcion == JFileChooser.APPROVE_OPTION) {
            String Nombre_archivo = filechoose.getSelectedFile().getPath();
            String rutaArchivoExcel = filechoose.getSelectedFile().toString();
            try {
                mySopaBinaria = new SopaBinaria(rutaArchivoExcel);
                JOptionPane.showMessageDialog(this, "Se ha cargado la sopa con exito");;
            } catch (IOException ex) {
                Logger.getLogger(GuiBinaria.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private void btnNumeroActionPerformed(java.awt.event.ActionEvent evt) throws Exception {
        int decimal = Integer.parseInt(NumeroDecimal.getText());
        if (decimal < 0) {
            throw new Exception("Error el numero debe ser positivo");
        }
        if (decimal > 999999999) {
            throw new Exception("Error el numero debe ser menor a 999999999");
        }
        String binario = mySopaBinaria.binario1(decimal);
        int total;
        total = mySopaBinaria.getTotal(decimal);
        txtImprimir.setText("Se encontro del número decimal: " + decimal + " en binario: " + binario + " Un total de " + total + " Veces");

    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) throws Exception {

        mySopaBinaria.crearInforme_PDF();

    }

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GuiBinaria().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JTextField NumeroDecimal;
    private javax.swing.JButton btnCargar;
    private javax.swing.JButton btnNumero;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtImprimir;
    // End of variables declaration                   
}
